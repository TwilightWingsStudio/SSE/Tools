/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include <Game/Game.h>

#ifdef GAMEGUI_EXPORTS
  #define GAMEGUI_API __declspec(dllexport)
#else
  #define GAMEGUI_API __declspec(dllimport)
#endif

// This class is exported from the GameGUI.dll
class CGameGUI
{
  public:
    // functions called from World Editor
    GAMEGUI_API static void OnInvokeConsole(void);
    GAMEGUI_API static void OnPlayerSettings(void);
    GAMEGUI_API static void OnAudioQuality(void);
    GAMEGUI_API static void OnVideoQuality(void);
    GAMEGUI_API static void OnSelectPlayerAndControls(void);
};

// global game gui object
extern CGameGUI _GameGUI;
