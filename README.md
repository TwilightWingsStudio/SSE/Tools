Tools for the **Serious Engine E**
================================================================================
Serious Engine E is the unofficial fork of Serious Engine 1.

All the tools and their dependencies bundled into this repository sepratelly from Engine.

Components:
--------------------------------------------------------------------------------
 * Tools
   * `EngineGUI` - GUI components for common engine things.
   * `GameGUI` - GUI components related to gameplay.
   * `LWSkaExporter` - Exporter plugin for the Lightwave 3D.
   * `MakeFONT` - Command line tool for making *.fnt (font data) files.
   * `RCon` - Remote console for DedicatedServer control.
   * `Modeler` - Program for creating MDL models.
   * `WorldEditor` - Map editor program.
   * `SkaStudio` - Program for creating Skeletal meshes.

Every project folder have project files for VIsual Studio 2013.

License
--------------------------------------------------------------------------------

Tools' source code is licensed under the GNU GPL v2 (see LICENSE file).

Some of the code included with the engine sources is not licensed under the GNU GPL v2:

 * LightWave SDK (located in `LWSkaExporter/SDK`) by NewTek Inc.
